﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TimeCalc
{
    public partial class Window : Form
    {

        private int minusVal = 0;
        private int plusVal = 0;

        public Window()
        {
            InitializeComponent();
        }

        private void Window_Load(object sender, EventArgs e)
        {
            this.timenow.Text = DateTime.Now.ToLongTimeString();
            this.timenow2.Text = DateTime.Now.ToLongTimeString();

            foreach (Control c in this.Controls)
            {
                if(c is MdiClient)
                {
                    c.BackColor = Control.DefaultBackColor;
                    this.Update();
                }
                c.Click += new EventHandler(updateResult);
            }
            this.Click += new EventHandler(updateResult);

        }

        private void updateResult(object sender, EventArgs e)
        {
            
            //get the last changed version.

            if(this.minusVal != (int)minhrs.Value)
            {
                this.minusVal = Decimal.ToInt32(this.minhrs.Value);
                DateTime origtime = DateTime.Parse(this.timenow.Text);
                DateTime time = origtime.AddHours(-this.minusVal);
                result.Text = time.ToLongDateString() + " " + time.ToLongTimeString();
            }
            if (this.plusVal != (int)plushrs.Value)
            {
                this.plusVal = Decimal.ToInt32(this.plushrs.Value);
                DateTime origtime = DateTime.Parse(this.timenow2.Text);
                DateTime time = origtime.AddHours(this.plusVal);
                result.Text = time.ToLongDateString() + " " + time.ToLongTimeString();
            }
        }

        private void Updateclock_Click(object sender, EventArgs e)
        {
            this.timenow.Text = DateTime.Now.ToLongTimeString();
            this.timenow2.Text = DateTime.Now.ToLongTimeString();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("TimeCalc version "+Application.ProductVersion+"\n\nDeveloped By Guido Lucassen and is licensed by GPLv3", "info");
        }
    }
}
