# TimeCalcSetup

## how to setup TimeCalcSetup with visualstudio?
1. first go ahead and download the wixtoolset at https://www.wixtoolset.org make sure you also download the visualstudio extension.. for visualstudio 2019 you may find this extension under release candidates.
2. once you have installed the wixtoolset you click on the visualstudio extension and install it.
3. when you completed step 1 and 2, you should start visualstudio and then go to the tab `Extension>Manage Extensions` then click on `online` and hit the searchbox and install this extension.
4. now you can open the sln project and it will open two projects the setup and the normal project.